<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 13.07.18
 * Time: 20:14
 */

namespace App\Controller;


use App\Entity\Product;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    /**
     * @Route("/")
     *
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, ObjectManager $manager){

        $product = new Product();
        $form = $this->createForm('App\Form\ProductType',$product);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $manager->persist($product);
            $manager->flush();
        }

        return $this->render('index.html.twig',[
            'form' => $form->createView()
        ]);
    }

}