<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 13.07.18
 * Time: 20:09
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile', FileType::class)
            ->add('Сохранить', SubmitType::class,array('label' => 'Опубликовать:'));
    }
}